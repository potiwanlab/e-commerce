package src.constants;

public class Constants {

    public static enum EState {
        MODE,
        LIST,
        CART,
        CHECKOUT
    };

    public static enum Mode {
        NONE,
        USER,
        ADMIN,
    }
}
