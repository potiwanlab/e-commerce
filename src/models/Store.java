package src.models;

import java.util.HashMap;

public class Store {

    public HashMap<String, Product> products;

    public Store() {
        products = new HashMap<>();
    }

    public void add(Product product) {
        products.put(product.getId(), product);
    }

    public void remove(String id) {
        products.remove(id);
    }
}
