package src.services;

import src.constants.Constants;
import src.constants.Constants.*;

public class StateManager extends StateManagerAbstact {

    private static StateManager instance;

    public static StateManager Instance() {
        if (instance == null) {
            instance = new StateManager();
            currentState = Constants.EState.MODE;
            curreMode = Constants.Mode.NONE;
        }
        return instance;
    }

    @Override
    public EState getState() {
        return currentState;
    }

    @Override
    public Mode getMode() {
        return curreMode;
    }

    @Override
    public void updateMode(Mode mode) {
        curreMode = mode;
    }

    @Override
    public void updateState(EState state) {
        currentState = state;
    }

}
