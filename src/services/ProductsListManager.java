package src.services;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Scanner;

import javax.lang.model.element.Element;

import src.constants.Constants;
import src.models.Cart;
import src.models.Product;
import src.models.Products;

public class ProductsListManager {

    private static StateManager stateManager;
    private Products products;
    private Cart cart;

    public ProductsListManager() {
        stateManager = StateManager.Instance();
        products = new Products();
        cart = new Cart();
    }

    public boolean addProduct(String id) {

        boolean sucess = false;
        if (stateManager.getMode().equals(Constants.Mode.USER)) {
            if (stateManager.getState().equals(Constants.EState.LIST)) {
                try {
                    cart.add(products.getProduct(id));
                    sucess = true;
                } catch (Exception e) {
                    System.out.println("Can't add same product");
                }
            } else {
                System.out.println("Not in LIST state");
            }

        } else {
            System.out.println("Your role can't add product to cart");
        }
        return sucess;
    }

    public boolean addProduct(String id, String name, double price) {

        boolean sucess = false;
        if (stateManager.getMode().equals(Constants.Mode.ADMIN)) {

            if (stateManager.getState().equals(Constants.EState.LIST)) {
                try {
                    products.add(new Product(id, name, price));
                    sucess = true;
                } catch (Exception e) {
                    System.out.println("Can't add same product");
                }
            } else {
                System.out.println("Not in LIST state");
            }

        } else {
            System.out.println("Your role can't add new product");
        }
        return sucess;
    }

    public boolean removeProduct(String id) {

        boolean sucess = false;
        if (stateManager.getMode().equals(Constants.Mode.ADMIN)) {

            if (stateManager.getState().equals(Constants.EState.LIST)) {
                try {
                    products.remove(id);
                    sucess = true;
                } catch (Exception e) {
                    System.out.println("Not found product");
                }
            } else {
                System.out.println("Not in LIST state");
            }

        } else if (stateManager.getMode().equals(Constants.Mode.USER)) {

            if (stateManager.getState().equals(Constants.EState.CART)) {
                try {
                    cart.remove(id);
                    sucess = true;
                } catch (Exception e) {
                    System.out.println("Not found product in cart");
                }
            } else {
                System.out.println("Not in CART state");
            }

        } else {
            System.out.println("Your role can't remove product");
        }

        return sucess;
    }

    public void printProducts() {
        for (Product product : products.products.values()) {
            System.out.println(product.getInfo());
        }
    }

    public void printCart() {
        for (Product product : cart.products.values()) {
            System.out.println(product.getInfo());
        }
    }
}
