package src.services;

import src.constants.Constants.EState;
import src.constants.Constants.Mode;

abstract class StateManagerAbstact {

    protected static EState currentState;
    protected static Mode curreMode;

    public abstract EState getState();

    public abstract Mode getMode();

    public abstract void updateMode(Mode mode);

    public abstract void updateState(EState state);
}
