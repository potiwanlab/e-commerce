import java.io.UTFDataFormatException;
import java.util.Scanner;

import javax.security.auth.login.FailedLoginException;

import src.constants.Constants;
import src.constants.Constants.EState;
import src.constants.Constants.Mode;
import src.services.ProductsListManager;
import src.services.StateManager;

public class AppManager {

    private static ProductsListManager productsListManager;
    private static StateManager stateManager;
    private static Scanner scanner;

    public static void main(String[] args) {

        stateManager = StateManager.Instance();
        scanner = new Scanner(System.in);
        productsListManager = new ProductsListManager();
        initProductsTest();

        initCommand();

    }

    private static void initProductsTest() {
        update(Constants.EState.LIST, Constants.Mode.ADMIN);
        productsListManager.addProduct("1", "test1", 100);
        productsListManager.addProduct("2", "test2", 200);
        productsListManager.addProduct("3", "test3", 300);
        productsListManager.addProduct("4", "test4", 123.54);
        productsListManager.addProduct("5", "test5", 123.42);
        update(Constants.EState.MODE, Constants.Mode.NONE);
    }

    private static void initCommand() {

        System.out.println("What do you want to do?");
        System.out.println("- User preesed 1");
        System.out.println("- Manager preesed 2");
        update(Constants.EState.MODE, Constants.Mode.NONE);

        String input = scanner.nextLine();
        if (!input.equals("1") && !input.equals("2")) {
            initCommand();
        } else {
            if (input.equals("1")) {
                userCommand();
            } else {
                adminCommand();
            }
        }
    }

    private static void userCommand() {
        System.out.println("List of products");

        update(Constants.EState.LIST, Constants.Mode.USER);

        productsListManager.printProducts();

        System.out.println("Back pressed b");
        System.out.println("What do you want to do?");
        System.out.println("- Add product to cart preesed 1");
        System.out.println("- Show and Remove product in cart preesed 2");

        String input = scanner.nextLine();
        if (!input.equals("1") && !input.equals("2")) {

            if (input.equals("b")) {
                initCommand();
            } else {
                userCommand();
            }
        } else {

            if (input.equals("1")) {
                userAddProduct();
            } else {
                userShowCart();
            }
        }
    }

    private static void adminCommand() {
        System.out.println("List of products");

        update(Constants.EState.LIST, Constants.Mode.ADMIN);

        productsListManager.printProducts();

        System.out.println("Back pressed b");
        System.out.println("What do you want to do?");
        System.out.println("- Add new product 1");
        System.out.println("- Remove product 2");

        String input = scanner.nextLine();
        if (!input.equals("1") && !input.equals("2")) {

            if (input.equals("b")) {
                initCommand();
            } else {
                adminCommand();
            }

        } else {

            if (input.equals("1")) {
                adminAddProduct();
            } else {
                adminRemovePRoduct();
            }
        }
    }

    private static void userAddProduct() {

        System.out.println("Back pressed b");
        System.out.println("What product do you want to add");
        System.out.print("Id: ");

        String input = scanner.nextLine();

        if (input.equals("b")) {
            userCommand();
        } else {
            boolean success = productsListManager.addProduct(input);

            if (success) {
                userCommand();
            } else {
                userAddProduct();
            }
        }
    }

    private static void userShowCart() {

        update(Constants.EState.CART, Constants.Mode.USER);
        System.out.println("List of product in your cart");
        productsListManager.printCart();

        System.out.println("Back pressed b");
        System.out.println("What product do you want to remove");
        System.out.print("Id: ");

        String input = scanner.nextLine();

        if (input.equals("b")) {
            userCommand();
        } else {
            productsListManager.removeProduct(input);
            userShowCart();
        }
    }

    private static void adminAddProduct() {

        System.out.println("Add info for new product");
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Name: ");
        String name = scanner.nextLine();
        System.out.print("Price: ");
        double price = scanner.nextDouble();

        productsListManager.addProduct(id, name, price);
        adminCommand();
    }

    private static void adminRemovePRoduct() {

        System.out.println("Back pressed b");
        System.out.println("What product do you want to remove");
        System.out.print("Id: ");

        String input = scanner.nextLine();

        if (input.equals("b")) {
            adminCommand();
        } else {
            productsListManager.removeProduct(input);
            adminCommand();
        }
    }

    private static void update(EState state, Mode mode) {
        stateManager.updateMode(mode);
        stateManager.updateState(state);
    }
}
